from utils.utils import get_config, get_model_path, save_json, skip_lines
from utils.picture_preparation.prepare_picture import get_pixel_from_image
from tensorflow import keras
import argparse
import tkinter as tk
from tkinter import *
from tkinter import filedialog


def load_model():
    """
    Returns the model from the saved model dir
    """

    model_config = get_config('model_config')

    return keras.models.load_model(
        get_model_path()
        / 'saved_models'
        / model_config['model_name']
    )

def predict_age(path_image):
    """
    Predicts age given image path
    """

    model = load_model()
    save_json('demo/model_meta/model_meta.json', model.to_json())
    picture = get_pixel_from_image(path_image)

    age_prediction = int(model.predict(picture, verbose=0)[0][0])

    skip_lines(4)

    print(f'The predicted age is : {age_prediction} ')
    print('Picture used for prediction is saved in demo/output_image/predicted_image.jpg')

    skip_lines(4)

    print("To try with your own picture :")

    skip_lines(1)

    print("python age_predictor/predict_age.py --path path/to/image")
    
    skip_lines(1)

    print("Or :")

    skip_lines(1)

    print("python age_predictor/predict_age.py --upload")
    
    skip_lines(2)

    return age_prediction

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("--path", type=str, help="Enter path to picture", default='demo/predict_my_age.jpg')
    parser.add_argument("--upload", help="Upload your picture with --upload", action='store_true')
    args = parser.parse_args()

    if args.upload:
        predict_age(filedialog.askopenfilename())
    
    else :
        predict_age(args.path)
