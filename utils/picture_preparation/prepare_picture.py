import cv2
import numpy as np

def rgb2gray(rgb):
    '''
    Converts an rgb image to a grayscale image
    '''
    return np.dot(rgb[...,:3], [0.2989, 0.5870, 0.1140])

def get_pixel_from_image(original_image_path='demo/predict_my_age.jpg'):
    '''
    Extracts from a picture the face, and returns the correct grayscale rgb shape of pixel for predicting with model
    '''

    original_image = cv2.imread(original_image_path)
    cv2.imwrite('demo/output_image/original_predicted_image.jpg', original_image)
    gray = cv2.cvtColor(original_image, cv2.COLOR_BGR2GRAY)

    faceCascade = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml")
    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.3,
        minNeighbors=3,
        minSize=(30, 30)
    )

    for (x, y, w, h) in faces:
        extracted_face = original_image[y+1:y + h, x+1:x + w]
        cv2.imwrite('demo/output_image/predicted_image.jpg', extracted_face)
        final_image = cv2.resize(rgb2gray(extracted_face), (48,48), interpolation = cv2.INTER_AREA)
        return final_image.reshape(1,48,48,1)/255
