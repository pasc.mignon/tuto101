import pandas as pd
import numpy as np
from utils.utils import get_config

def generate_pixel_array(list_pixels):
    """
    Returns an 2 dimension array of pixels given a list of pixels
    """

    pixel_array = np.array(list_pixels.split(), dtype="float32")
    new_dimension = int(np.sqrt(len(pixel_array)))
    pixel_array = np.asarray(pixel_array).astype(np.float32)
    pixel_array = pixel_array.reshape((new_dimension, new_dimension, 1))

    return np.asarray(pixel_array).astype(np.float32)


def clean_raw_data(raw_dataset, config_dict):
    '''
    Returns a clean dataset from raw_data
    '''

    clean_df = raw_dataset.copy()
    clean_df = clean_df[list(config_dict['columns_to_keep'].values())]
    
    clean_df[config_dict['columns_to_keep']["pixel_column"]] = clean_df[config_dict['columns_to_keep']["pixel_column"]].apply(
        lambda list_pixel : generate_pixel_array(list_pixel)
    )

    clean_df[config_dict['columns_to_keep']["pixel_column"]] = clean_df[config_dict['columns_to_keep']["pixel_column"]].apply(
        lambda x: x/255
    )

    return clean_df


def generate_dataset():
    """
    Generates the dataset for training the model
    """
    
    config_dict = get_config("data_config")
    raw_data = pd.read_csv(config_dict["raw_data_path"])

    return clean_raw_data(raw_data, config_dict)