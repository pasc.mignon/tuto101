from pathlib import Path
import yaml
import json

def get_project_path():
    '''
    Returns project path
    '''
    return Path.cwd()

def get_data_path():
    '''
    Returns data path
    '''
    return get_project_path() / Path('data')

def get_config_path():
    '''
    Returns config path
    '''
    return get_project_path() / Path('config')

def get_utils_path():
    '''
    Returns utils path
    '''
    return get_project_path() / Path('utils')

def get_model_path():
    '''
    Returns model path
    '''
    return get_project_path() / Path('model')

def get_config(config_name):
    '''
    Returns a dict from a yaml config file
    '''

    config_path = get_config_path()

    with open((config_path / f"{config_name}.yaml"), 'r') as yaml_file:
        try:
            return yaml.safe_load(yaml_file)
        except yaml.YAMLError as exc:
            return exc


def skip_lines(number_of_lines=2):
    """
    Skips lines when printing on a terminal
    """
    for i in range(number_of_lines):
        print('')

def save_json(path_to_save, data):
    """
    Saves a json into the given path
    """
    with open(get_project_path() / path_to_save, 'w') as f:
        json.dump(data, f)





