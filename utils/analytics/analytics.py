import matplotlib.pyplot as plt
import numpy as np

from utils.data_preparation.generate_dataset import generate_dataset

def plot_picture(dataset):
    """
    Plots a single picture randomly
    """

    random_index = np.random.randint(len(dataset))

    age = dataset.age[random_index]
    image = dataset.pixels[random_index]

    plt.imshow(image, cmap='gray')
    plt.title(f'Age:{age}')

def make_plots(number_of_plots=1):
    """
    Plots as subplots a given number of random pictures from dataset
    """

    dataset = generate_dataset()
    subplots_dim = int(np.sqrt(number_of_plots))

    figure = plt.figure(figsize=(10,10))

    for i in range(number_of_plots):

        figure.add_subplot(subplots_dim + 1, subplots_dim, i+1)

        plot_picture(dataset)
    
    plt.show()


if __name__ == '__main__':
    make_plots(12)