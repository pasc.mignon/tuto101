import tensorflow as tf
import tensorflow.keras.layers as L
from sklearn.model_selection import train_test_split
import numpy as np

from datetime import datetime

from utils.data_preparation.generate_dataset import generate_dataset
from utils.utils import get_config, get_model_path

def get_age_predictor_model():
    """
    Generates a tf model
    """
    
    model = tf.keras.Sequential([
        L.InputLayer(input_shape=(48,48,1)),
        L.Conv2D(32, (3, 3), activation='relu', input_shape=(32, 32, 3)),
        L.BatchNormalization(),
        L.MaxPooling2D((2, 2)),
        L.Conv2D(64, (3, 3), activation='relu'),
        L.MaxPooling2D((2, 2)),
        L.Conv2D(128, (3, 3), activation='relu'),
        L.MaxPooling2D((2, 2)),
        L.Flatten(),
        L.Dense(64, activation='relu'),
        L.Dropout(rate=0.5),
        L.Dense(1, activation='relu')
    ])

    model.compile(optimizer='adam',
                loss='mean_squared_error',
                metrics=['mae'])

    return model

def get_train_test_data():
    """
    Returns X and y for train and test
    """

    data_config_dict = get_config("data_config")
    dataset = generate_dataset()

    X_train, X_test, y_train, y_test = train_test_split(
       np.array(dataset[data_config_dict['columns_to_keep']["pixel_column"]].tolist()), 
       dataset[data_config_dict['columns_to_keep']["age_column"]], 
       test_size=0.15
    )

    return X_train, X_test, y_train, y_test


def fit_model():
    """
    Fits the model to train data, tests on testing data and saves the model to the saved_model directory
    """

    model_config_dict = get_config("model_config")
    X_train, X_test, y_train, y_test = get_train_test_data()
    model = get_age_predictor_model()

    model.fit(
        X_train, 
        y_train, 
        epochs=model_config_dict["epochs"], 
        batch_size=model_config_dict["batch_size"],
    )

    mse, mae = model.evaluate(X_test,y_test,verbose=0)
    print(f'Mae on test data : {mae}')

    model.save((
        get_model_path() 
        / 'saved_models' 
        / f'{datetime.now().strftime("%m%d%Y_%H%M%S")}__epochs_{model_config_dict["epochs"]}__batch_size_{model_config_dict["batch_size"]}__mae_{mae}'  
    ))

    return model