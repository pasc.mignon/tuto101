# Project for P&S class - Pascal Mignon

This project aims at predicting the age of people based on image analysis.

## Installation and usage

Clone the repo

```bash
git clone git@gitlab.ethz.ch:python22/pmignon.git
```

Enter project repo

```bash
cd pmignon
```

Install packages

```bash
pip install -r requirements.txt
```

Create local setup package

```bash
pip install -e .
```

Run predictor

```bash
python age_predictor/predict_age.py
```

## Results

The age predictor can predict the age of a picture given by Path or by uploading a picture.
The model used has a mae of 7 on the test set.

## Contributing and improvements

Next steps on this project could be:

- Improving the model performance
- Adding webcam feature to take directly picture from webcam

## Contact
- pmignon@student.ethz.ch