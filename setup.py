from setuptools import setup, find_packages

setup(
   version='1.0',
   packages=find_packages(include=['.*']),
   name='pmignon',
   description='Project for PS class - age predictor',
   author='Pascal MIGNON',
   author_email='pmignon@student.ethz.ch',
)
